FROM scratch

ADD rootfs /
ADD qemu-*-static /usr/bin

RUN pacman-key --init
RUN pacman-key --populate archlinuxarm

CMD [ "/bin/sh" ]
