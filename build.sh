#!/bin/sh

wget http://os.archlinuxarm.org/os/ArchLinuxARM-rpi-latest.tar.gz
mkdir ./rootfs
bsdtar -xpf ArchLinuxARM-rpi-latest.tar.gz -C rootfs
sync

docker build .
